package com.toltecayotl.interview.skimmiatest.ui.gallery

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.toltecayotl.interview.skimmiatest.R
import com.toltecayotl.interview.skimmiatest.data.SongListData
import com.toltecayotl.interview.skimmiatest.databinding.FragmentProfileBinding
import com.toltecayotl.interview.skimmiatest.databinding.PictureBrowserPagerBinding
import com.toltecayotl.interview.skimmiatest.helpers.SharedPreferencesUtil
import com.toltecayotl.interview.skimmiatest.ui.Player
import com.toltecayotl.interview.skimmiatest.ui.playMusic
import com.toltecayotl.interview.skimmiatest.ui.player.PlayerFragment
import com.toltecayotl.interview.skimmiatest.ui.profile.ProfileFragment
import kotlinx.android.synthetic.main.layout_list.view.*
import java.io.File
import java.io.IOException

class GalleryFragment : Fragment() {

    private lateinit var binding: PictureBrowserPagerBinding
    private lateinit var galleryViewModel: GalleryViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.picture_browser_pager!!, container, false)
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }
    private fun setupView() {
        requestPermissionsGallery()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ProfileFragment.REQUEST_PICK_PHOTO -> pickPhoto(data)
            }
        }
    }
    fun loadImg(path:String){
        if (Build.VERSION.SDK_INT >= 29) {
            try {
                requireContext().contentResolver.openFileDescriptor(Uri.fromFile(File(path)), "r").use({ pfd ->
                    if (pfd != null) {
                        var bitmap = BitmapFactory.decodeFileDescriptor(pfd.getFileDescriptor())
                        binding.image.setImageBitmap(bitmap)
                    }
                })
            } catch (ex: IOException) {
            }

        }else{
            val bitmapFile =   File(path)
            val myBitmap = BitmapFactory.decodeFile(bitmapFile.getAbsolutePath())

//                var albumImageRounded = RoundedBitmapDrawableFactory.create(resources, bitmap)
//                albumImageRounded.setCornerRadius(Math.max(albumImageRounded.bitmap!!.width,
//                    albumImageRounded.bitmap!!.height) / 1.0f);

            binding.image.setImageBitmap(myBitmap)
        }
    }

    private fun pickPhoto(data: Intent?) {
        data?.let {
            val uri = it.data

            try {
                val options = BitmapFactory.Options()
                options.inJustDecodeBounds = false
                options.inPreferredConfig = Bitmap.Config.RGB_565

                loadImg(getRealPathFromURI(uri))

//                when (exif.getAttribute(ExifInterface.TAG_ORIENTATION)) {
//                    "6" -> bitmap = bitmap rotate 90
//                    "8" -> bitmap = bitmap rotate 270
//                    "3" -> bitmap = bitmap rotate 180
//                }
//                if (onPickerCloseListener != null) {
//                    onPickerCloseListener?.onPickerClosed(ItemModel.ITEM_GALLERY, bitmap.toUri(context!!, fileName))
//                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
    private fun requestPermissionsGallery() {
        context?.let {
            if (ActivityCompat.checkSelfPermission(it, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(it, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(requireActivity(),
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    ProfileFragment.REQUEST_PERMISSION_GALLERY
                )
            } else {
                openGallery()
            }
        }
    }
    fun getRealPathFromURI(uri:Uri? ):String {

        var cursor = requireContext().contentResolver.query(uri!!, null, null, null, null);
        var  column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private fun openGallery() {
        val pickPhoto = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        try {
            startActivityForResult(pickPhoto, ProfileFragment.REQUEST_PICK_PHOTO)
        } catch (e: ActivityNotFoundException) {
            Log.e("Picker", "Cannot open Gallery", e)
        }
    }

}
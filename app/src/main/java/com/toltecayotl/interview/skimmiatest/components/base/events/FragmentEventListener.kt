package com.speedymovil.wire.components.base

interface FragmentEventListener {
    fun EventNotification(Sender: Any, Action: FragmentEventType)
}
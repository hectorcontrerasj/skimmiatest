package com.speedymovil.wire.components.forms.input

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.InputFilter
import android.text.Spanned
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import com.speedymovil.wire.components.base.FragmentEventListener
import com.speedymovil.wire.components.base.FragmentEventType
import com.toltecayotl.interview.skimmiatest.R
import com.toltecayotl.interview.skimmiatest.databinding.InputTextFormBinding


class InputTextForm @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ConstraintLayout(context, attrs) {

    var binding: InputTextFormBinding
    var listener:FragmentEventListener? = null

    private  var radio =10;
    init {
        val layoutInflater = getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = InputTextFormBinding.inflate(layoutInflater, this, true)

        brandComponents(attrs)


        setWillNotDraw(false)


    }

    @SuppressLint("Recycle")
    fun brandComponents(attrs: AttributeSet?)
    {
        val a: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.ds)
        binding.title.text = a.getString(R.styleable.ds_input_title)
        when(a.getString(R.styleable.ds_input_title_visibility)){
            "visible" -> binding.title.visibility =  View.VISIBLE
            "gone" -> binding.title.visibility =  View.GONE
            "invisible" -> binding.title.visibility =  View.INVISIBLE
        }

        when(a.getString(R.styleable.ds_input_type)){
            "email","token" -> binding.text.filters = arrayOf(EmojiExcludeFilter(),BlockCharacterFilter())
            "phone" -> {binding.text.inputType=EditorInfo.TYPE_CLASS_PHONE
                binding.text.filters = arrayOf(BlockCharacterFilter(), OnlyNumberFilter())}

        }

        a.recycle()

    }

    fun setListenerExternal(listener:FragmentEventListener?) {
        binding.text.addTextChangedListener (
            object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                        listener?.EventNotification(this, FragmentEventType.REFRESH(p0.toString()))
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }
            }
        )
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas?) {

        val path = Path();
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = resources.getColor(R.color.gray_alert)
        paint.strokeWidth = 2f
        paint.style = Paint.Style.STROKE

        path.moveTo(binding.text.left.toFloat()+radio,binding.text.top.toFloat())
        path.lineTo(binding.title.left.toFloat(),binding.text.top.toFloat())
        path.moveTo(binding.title.right.toFloat(),binding.text.top.toFloat())
        path.lineTo(binding.text.right.toFloat()-radio,binding.text.top.toFloat())

        canvas!!.drawPath(path, paint)
        canvas.save()

        path.moveTo(binding.text.right.toFloat()-radio,binding.text.top.toFloat())

        //Curva Lado izquierdo superior
        path.cubicTo(
            binding.text.right.toFloat(),binding.text.top.toFloat(),
            binding.text.right.toFloat(), (binding.text.top.toFloat() +radio),
            binding.text.right.toFloat(), (binding.text.top.toFloat() +radio)
        )
        path.moveTo(binding.text.right.toFloat(),binding.text.top.toFloat()+radio)

        //Linea entre la curva
        path.lineTo(binding.text.right.toFloat(),binding.text.bottom.toFloat()-radio)

        path.moveTo(binding.text.right.toFloat(),binding.text.bottom.toFloat()-radio)

        //Curva 2 Lado izquierdo inferior
        path.cubicTo(
            binding.text.right.toFloat(),binding.text.bottom.toFloat(),
            binding.text.right.toFloat()-radio, (binding.text.bottom.toFloat()),
            binding.text.right.toFloat()-radio, (binding.text.bottom.toFloat())
        )

        canvas.drawPath(path, paint)
        canvas.save()
        //Curva 2 Linea
        path.moveTo(binding.text.right.toFloat()-radio,(binding.text.bottom.toFloat()))

        path.lineTo(binding.text.left.toFloat() +radio,binding.text.bottom.toFloat())
        path.moveTo(binding.text.left.toFloat() +radio,binding.text.bottom.toFloat())

        //Curva 2 Lado Derecho inferior

        path.cubicTo(
            binding.text.left.toFloat(),binding.text.bottom.toFloat(),
            binding.text.left.toFloat(), (binding.text.bottom.toFloat()-radio),
            binding.text.left.toFloat(), (binding.text.bottom.toFloat()-radio)
        )

        canvas.drawPath(path, paint)
        canvas.save()

        path.moveTo(binding.text.left.toFloat(), (binding.text.bottom.toFloat()-radio))
        path.lineTo(binding.text.left.toFloat(),(binding.text.top.toFloat()+radio))
        path.moveTo(binding.text.left.toFloat(),(binding.text.top.toFloat()+radio))


       // path.lineTo(binding.text.left.toFloat(),(binding.text.top.toFloat()))

        //path.lineTo( binding.text.left.toFloat()+radio, (binding.text.top.toFloat()))


        path.cubicTo(
            binding.text.left.toFloat(),(binding.text.top.toFloat()),
            binding.text.left.toFloat()+radio, (binding.text.top.toFloat()),
            binding.text.left.toFloat()+radio, (binding.text.top.toFloat())
        )
        canvas!!.drawPath(path, paint)
        canvas.save()



    }

    private class BlockCharacterFilter: InputFilter {

        private val blockCharacterSet = "~#^|$%&*!:"

        override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence? {
            return if (source != null && blockCharacterSet.contains("" + source)) {
                ""
            } else null
        }

    }

    private class OnlyNumberFilter: InputFilter {

        private val blockCharacterSet = "abcdefghijklmnñopqrsuvwxyz"

        override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence? {
            return if (source != null && blockCharacterSet.contains("" + source)) {
                ""
            } else null
        }

    }

    private inner class EmojiExcludeFilter : InputFilter {

        override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
            for (i in start until end) {
                val type = Character.getType(source[i])
                if (type == Character.SURROGATE.toInt() || type == Character.OTHER_SYMBOL.toInt()) {
                    return ""
                }
            }
            return null
        }
    }

    fun setInputType(inputType: Int){
        binding.text.setRawInputType(inputType)
    }

    fun getText(): String{
        return binding.text.text.toString()
    }

    fun setText(text:String?){
        binding.text.setText(text)
    }

    fun setMaxLength(length: Int){
        val fArray = arrayOfNulls<InputFilter>(1)
        fArray[0] = InputFilter.LengthFilter(length)
        binding.text.filters = fArray
    }

    fun setInputTypeDigits(input: Int){
        binding.text.inputType = input
    }

    /**
     * This method is used to set a [action]
     * when "editText" text changes.
     */
    fun onTextChanged(action: (editable: Editable?) -> Unit) {

        binding.text.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
                action.invoke(s)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // Not necessary
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Not necessary
            }

        })
    }
}

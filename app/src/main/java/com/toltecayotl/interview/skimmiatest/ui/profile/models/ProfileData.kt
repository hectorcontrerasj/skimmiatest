package com.toltecayotl.interview.skimmiatest.ui.profile.models

data class ProfileData (
    var name: String? = null,
    var lastName: String? = null,
    var bio: String? = null)
package com.toltecayotl.interview.skimmiatest.data

import com.toltecayotl.interview.skimmiatest.R

object SongListData {
    val listSongs: ArrayList<Song>
        get() = arrayListOf(
            Song(1, R.raw.slow_down,0,"Slow Down","Selena Gomez"),
            Song(2, R.raw.who_says,0,"Who Says","Selena Gomez"),
            Song(3, R.raw.come_get_it,0,"Come Get it","Selena Gomez")
        )
}
package com.toltecayotl.interview.skimmiatest.helpers


import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.toltecayotl.interview.skimmiatest.base.AppDelegate

class SharedPreferencesUtil {
    private val preferencesManager: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(
        AppDelegate.Instance!!.applicationContext
    )
    private val editor: SharedPreferences.Editor

    /**
     * Method that saves a String in Shared Preference
     *
     * @param key   The String that is going to be the key
     * @param value The String that is going to be saved
     */
    fun setAppPreference(key: String?, value: String?) {
        editor.putString(key, value)
        editor.commit()
    }

    /**
     * Method that saves a Integer in Shared Preference
     *
     * @param key   The String that is going to be the key
     * @param value The Integer that is going to be saved
     */
    fun setAppPreference(key: String?, value: Int) {
        editor.putInt(key, value)
        editor.commit()
    }

    /**
     * Method that saves a Boolean in Shared Preference
     *
     * @param key   The String that is going to be the key
     * @param value The String that is going to be saved
     */
    fun setAppPreference(key: String?, value: Boolean) {
        editor.putBoolean(key, value)
        editor.commit()
    }

    /**
     * Method that saves a Long in Shared Preference
     *
     * @param key   The String that is going to be the key
     * @param value The Long that is going to be saved
     */
    fun setAppPreference(key: String?, value: Long) {
        editor.putLong(key, value)
        editor.commit()
    }

    /**
     * Method that saves a Float in Shared Preference
     *
     * @param key   The String that is going to be the key
     * @param value The Long that is going to be saved
     */
    fun setAppPreference(key: String?, value: Float) {
        editor.putFloat(key, value)
        editor.commit()
    }

    /**
     * Method that returns a String from Shared Preference
     *
     * @param key The String that is going to be the key
     * @return String The string that was saved with the key
     */
    fun getStringPreference(key: String?): String? {
        return preferencesManager.getString(key, "")
    }

    /**
     * Method that returns a Integer from Shared Preference
     *
     * @param key The String that is going to be the key
     * @return The Integer that was saved with the key
     */
    fun getIntPreference(key: String?, defaultValue:Int=0): Int {
        return preferencesManager.getInt(key, defaultValue)
    }

    /**
     * Method that returns a Long from Shared Preference
     *
     * @param key The String that is going to be the key
     * @return The Long that was saved with the key
     */
    fun getLongPreference(key: String?): Long {
        return preferencesManager.getLong(key, 0)
    }

    /**
     * Method that returns a Long from Shared Preference
     *
     * @param key The String that is going to be the key
     * @return The Float that was saved with the key
     */
    fun getFloatPreference(key: String?): Float {
        return preferencesManager.getFloat(key, 0f)
    }

    /**
     * Method that returns a Boolean from Shared Preference
     *
     * @param key The String that is going to be the key
     * @return The Boolean that was saved with the key
     */
    fun getBooleanPreference(key: String?): Boolean {
        return preferencesManager.getBoolean(key, false)
    }

    fun clearAllPreferences() {
        editor.clear()
        editor.commit()
    }

    fun removePreference(key: String?) {
        editor.remove(key)
        editor.commit()
    }

    fun getPreferences(context: Context, app: String?): SharedPreferences {
        return context.getSharedPreferences(app, Context.MODE_PRIVATE)
    }

    fun hasPreference(key: String?): Boolean {
        return preferencesManager.contains(key)
    }

    companion object {
        private var mInstance: SharedPreferencesUtil? = null

        @JvmStatic
        val instance: SharedPreferencesUtil?
            get() {
                if (mInstance == null) mInstance = SharedPreferencesUtil()
                return mInstance
            }
    }

    init {
        editor = preferencesManager.edit()
    }
}
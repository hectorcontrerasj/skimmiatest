package com.toltecayotl.interview.skimmiatest.ui.player

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.toltecayotl.interview.skimmiatest.R
import com.toltecayotl.interview.skimmiatest.data.Song
import com.toltecayotl.interview.skimmiatest.databinding.LayoutListItemBinding

class SongsAdapter(val items: List<Song>, val listener: SelectedItemList) :
    RecyclerView.Adapter<SongsAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: LayoutListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_list_item, parent, false
        )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = items[position]
        holder.itemBinding.cantante.text = data.nameSinger
        holder.itemBinding.tituloPista.text = data.nameSong
        holder.itemBinding.root.setOnClickListener {
            listener?.let {
                listener.itemSelected(data.id)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    class MyViewHolder(val itemBinding: LayoutListItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root)
}
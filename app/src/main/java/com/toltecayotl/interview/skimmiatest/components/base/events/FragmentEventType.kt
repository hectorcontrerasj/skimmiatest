package com.speedymovil.wire.components.base

sealed class FragmentEventType {
    class SHOW_FRAGMENT(val fragmentName: String) : FragmentEventType()
    class OPEN_VIEW(val viewName: String) : FragmentEventType()
    class CHANGE_SECTION(val sectionName: String) : FragmentEventType()
    class REFRESH(val fragmentName: String) : FragmentEventType()

    class RADIO_SELECTED(val positionSelected: Int) : FragmentEventType()

}

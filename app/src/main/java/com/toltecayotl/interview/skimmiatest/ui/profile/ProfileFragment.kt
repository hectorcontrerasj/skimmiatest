package com.toltecayotl.interview.skimmiatest.ui.profile

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.toltecayotl.interview.skimmiatest.R
import com.toltecayotl.interview.skimmiatest.databinding.FragmentProfileBinding
import com.toltecayotl.interview.skimmiatest.helpers.SharedPreferencesUtil
import java.io.File
import java.io.IOException
import java.util.*

class ProfileFragment : Fragment() {

    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var binding: FragmentProfileBinding;
    private val keyName:String = "keyName"
    private val keyLastName:String = "keyLastName"
    private val keyURI:String = "URI"
    companion object {

        const val REQUEST_PERMISSION_GALLERY = 1002
        const val REQUEST_PICK_PHOTO = 1102

    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile!!, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    private fun setupView() {
        binding.userName.setText(SharedPreferencesUtil.instance!!.getStringPreference(keyName))
        binding.userLastName.setText(SharedPreferencesUtil.instance!!.getStringPreference(keyLastName))
        if(SharedPreferencesUtil.instance!!.getStringPreference(keyURI)!=""){
            loadImg(SharedPreferencesUtil.instance!!.getStringPreference(keyURI)!!)
        }
        binding.button.setOnClickListener {
            SharedPreferencesUtil.instance!!.setAppPreference(keyName,binding.userName.getText() )
            SharedPreferencesUtil.instance!!.setAppPreference(keyLastName, binding.userLastName.getText() )
            Toast.makeText(context, "Guardado", Toast.LENGTH_SHORT).show()
        }
        binding.photo.setOnClickListener{
            requestPermissionsGallery()

        }
    }

    fun changePhoto(uri: Uri?){
        uri?.let {
            val fileName = "${UUID.randomUUID()}.jpg"

//            PhotoUtils.clearDirectoryPhoto("photoUser")
//            val result= FileUtils.saveInternalFileImg(context!!.contentResolver, fileName, "photoUser", uri)
//            if (!result){
//
//                onErrorLiveData.value="Error al guardar imagen"
//                return
//            }
//            val raw = PhotoUtils.savePhotoWithUser(fileName)
//            if (raw>-1){
//                onSuccessLiveData.value=SuccessChangePhoto(fileName)
//
//
//            }else{
//                onErrorLiveData.value="Error al guardar imagen"
//
//            }
        }
    }


    private fun requestPermissionsGallery() {
        context?.let {
            if (ActivityCompat.checkSelfPermission(it, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(it, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(requireActivity(),
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_PERMISSION_GALLERY)
            } else {
                openGallery()
            }
        }
    }
    private fun openGallery() {
        val pickPhoto = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        try {
            startActivityForResult(pickPhoto, REQUEST_PICK_PHOTO)
        } catch (e: ActivityNotFoundException) {
            Log.e("Picker", "Cannot open Gallery", e)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_PICK_PHOTO -> pickPhoto(data)
            }
        }
    }

    fun loadImg(path:String){
        if (Build.VERSION.SDK_INT >= 29) {
            try {
                requireContext().contentResolver.openFileDescriptor(Uri.fromFile(File(path)), "r").use({ pfd ->
                    if (pfd != null) {
                        var bitmap = BitmapFactory.decodeFileDescriptor(pfd.getFileDescriptor())
                        binding.photo.setImageBitmap(bitmap)
                    }
                })
            } catch (ex: IOException) {
            }

        }else{
            val bitmapFile =   File(path)
            val myBitmap = BitmapFactory.decodeFile(bitmapFile.getAbsolutePath())

//                var albumImageRounded = RoundedBitmapDrawableFactory.create(resources, bitmap)
//                albumImageRounded.setCornerRadius(Math.max(albumImageRounded.bitmap!!.width,
//                    albumImageRounded.bitmap!!.height) / 1.0f);

            binding.photo.setImageBitmap(myBitmap)
        }
    }

    private fun pickPhoto(data: Intent?) {
        data?.let {
            val uri = it.data

            try {
                val options = BitmapFactory.Options()
                options.inJustDecodeBounds = false
                options.inPreferredConfig = Bitmap.Config.RGB_565

                loadImg(getRealPathFromURI(uri))
                SharedPreferencesUtil.instance!!.setAppPreference(keyURI, getRealPathFromURI(uri))
//                when (exif.getAttribute(ExifInterface.TAG_ORIENTATION)) {
//                    "6" -> bitmap = bitmap rotate 90
//                    "8" -> bitmap = bitmap rotate 270
//                    "3" -> bitmap = bitmap rotate 180
//                }
//                if (onPickerCloseListener != null) {
//                    onPickerCloseListener?.onPickerClosed(ItemModel.ITEM_GALLERY, bitmap.toUri(context!!, fileName))
//                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
    fun getRealPathFromURI(uri:Uri? ):String {

        var cursor = requireContext().contentResolver.query(uri!!, null, null, null, null);
        var  column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


//    override fun onPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
//        when (requestCode) {
//            REQUEST_PERMISSION_GALLERY -> if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
//                openGallery()
//            }
//        }
//    }
}
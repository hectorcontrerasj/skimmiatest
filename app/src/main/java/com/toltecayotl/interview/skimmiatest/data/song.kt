package com.toltecayotl.interview.skimmiatest.data

data class Song (val id:Int, val songfile:Int, val image:Int, val nameSong:String, val nameSinger:String)
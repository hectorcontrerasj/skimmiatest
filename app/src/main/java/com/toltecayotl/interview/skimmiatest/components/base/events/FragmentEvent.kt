package com.speedymovil.wire.components.base

import android.os.Bundle
import java.lang.reflect.Type

class FragmentEvent(type: Type, var arguments : Bundle)
package com.toltecayotl.interview.skimmiatest.ui.gallery

interface SelectedItemList {
    fun itemSelected(id:Int)
}
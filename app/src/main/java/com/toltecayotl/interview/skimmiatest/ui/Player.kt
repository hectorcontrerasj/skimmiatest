package com.toltecayotl.interview.skimmiatest.ui

import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.os.Bundle
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.RoundedBitmapDrawable
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.toltecayotl.interview.skimmiatest.R
import com.toltecayotl.interview.skimmiatest.data.Song
import com.toltecayotl.interview.skimmiatest.data.SongListData


class Player : AppCompatActivity() {
    lateinit var mediaPlayer: MediaPlayer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_player)
        val songid = intent.getIntExtra("song", 0)

        // implementar el Player

        var cancion: Song = SongListData.listSongs.filter { it.id == songid }?.get(0)
        val album = findViewById(R.id.album) as ImageView
        val nameSong = findViewById(R.id.titulo_pista) as TextView
        val nameArtist = findViewById(R.id.cantante) as TextView

        var albumImageRounded = RoundedBitmapDrawableFactory.create(resources,BitmapFactory.decodeResource(resources,R.drawable.portada))
        // Radio de la imagen
        albumImageRounded.setCornerRadius(Math.max(albumImageRounded.bitmap!!.width, albumImageRounded.bitmap!!.height) / 2.0f);
        album.setImageDrawable(albumImageRounded)
        R.drawable.portada

        nameSong.text = cancion.nameSong
        nameArtist.text = cancion.nameSinger

        if (cancion != null) {
            mediaPlayer = MediaPlayer.create(this, cancion.songfile)
            mediaPlayer?.start() // no need to call prepare(); create() does that for you
            val imgButton = findViewById(R.id.play_pause) as ImageButton
            val prevButton = findViewById(R.id.previo) as ImageButton
            val forwButton = findViewById(R.id.siguiente) as ImageButton
            prevButton.setOnClickListener{
                mediaPlayer.stop()
                if(cancion.id>1){
                    cancion = SongListData.listSongs.filter { it.id == (cancion.id-1) }?.get(0)
                    mediaPlayer = MediaPlayer.create(this, cancion.songfile)
                    mediaPlayer?.start()
                    nameSong.text = cancion.nameSong
                    nameArtist.text = cancion.nameSinger
                }
                else{
                    cancion = SongListData.listSongs.filter { it.id == (SongListData.listSongs.size) }?.get(0)
                    mediaPlayer = MediaPlayer.create(this, cancion.songfile)
                    mediaPlayer?.start()
                    nameSong.text = cancion.nameSong
                    nameArtist.text = cancion.nameSinger
                }

            }
            forwButton.setOnClickListener{
                mediaPlayer.stop()
                if(cancion.id < SongListData.listSongs.size){
                    cancion = SongListData.listSongs.filter { it.id == (cancion.id +1) }?.get(0)
                    mediaPlayer = MediaPlayer.create(this, cancion.songfile)
                    mediaPlayer?.start()
                    nameSong.text = cancion.nameSong
                    nameArtist.text = cancion.nameSinger
                }
                else{
                    cancion = SongListData.listSongs.filter { it.id == 1 }?.get(0)
                    mediaPlayer = MediaPlayer.create(this, cancion.songfile)
                    mediaPlayer?.start()
                    nameSong.text = cancion.nameSong
                    nameArtist.text = cancion.nameSinger
                }

            }
            imgButton.setImageResource(R.drawable.ic_pause)
            imgButton.setOnClickListener {
                if (mediaPlayer.isPlaying) {
                    mediaPlayer.pause()
                    imgButton.setImageResource(R.drawable.ic_play)
                } else {
                    mediaPlayer.start()
                    imgButton.setImageResource(R.drawable.ic_pause)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.stop()
    }
}
package com.toltecayotl.interview.skimmiatest.ui.profile

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream

class ProfileViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is notifications Fragment"
    }
    val text: LiveData<String> = _text

    fun readPicture(context: Context, path:String) : Drawable? {
        return  BitmapDrawable.createFromPath(path)
    }
}
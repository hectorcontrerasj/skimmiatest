package com.toltecayotl.interview.skimmiatest.ui.player

interface SelectedItemList {
    fun itemSelected(id:Int)
}
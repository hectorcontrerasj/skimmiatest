package com.toltecayotl.interview.skimmiatest.base

import android.annotation.SuppressLint
import android.app.Application
import android.provider.Settings.Secure
import android.provider.Settings.Secure.getString
import androidx.core.content.pm.PackageInfoCompat

class AppDelegate : Application() {
    companion object {


        var Instance: AppDelegate? = null
        var appVersionName: String = ""
        var appVersionCode: String = ""
        private var ANDROID_ID: String = ""

        @SuppressLint("HardwareIds")
        fun getAndroidId(): String {
            if (ANDROID_ID != null) return ANDROID_ID
            ANDROID_ID = getString(
                Instance?.contentResolver,
                Secure.ANDROID_ID
            )
            return ANDROID_ID
        }
    }


    override fun onCreate() {
        super.onCreate()
        //Singleton
        Instance = this


        val pInfo = packageManager.getPackageInfo(packageName, 0)
        appVersionName =packageManager.getPackageInfo(packageName, 0).versionName
        appVersionCode = PackageInfoCompat.getLongVersionCode(pInfo).toString()

    }


}
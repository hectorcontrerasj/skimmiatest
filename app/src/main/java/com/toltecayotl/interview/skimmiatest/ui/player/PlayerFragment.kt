package com.toltecayotl.interview.skimmiatest.ui.player

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.toltecayotl.interview.skimmiatest.R
import com.toltecayotl.interview.skimmiatest.data.SongListData
import com.toltecayotl.interview.skimmiatest.ui.Player
import com.toltecayotl.interview.skimmiatest.ui.gallery.SelectedItemList
import com.toltecayotl.interview.skimmiatest.ui.gallery.SongsAdapter
import kotlinx.android.synthetic.main.layout_list.view.*

class PlayerFragment : Fragment(), SelectedItemList {

    private lateinit var playerViewModel: PlayerViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        playerViewModel =
                ViewModelProviders.of(this).get(PlayerViewModel::class.java)
        val root = inflater.inflate(R.layout.layout_list, container, false)
        root.listSongs.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        root.listSongs.adapter = SongsAdapter(SongListData.listSongs,this)//        val textView: TextView = root.findViewById(R.id.text_home)
//        playerViewModel.text.observe(viewLifecycleOwner, Observer {
//            textView.text = it
//        })
        initView()
        return root
    }

    private fun initView() {
    }

    override fun itemSelected(id: Int) {
        val intent = Intent(context, Player::class.java)
        intent.putExtra("song",id)
        startActivity(intent)
    }
}